import React, { useState } from 'react';
import { Link } from 'react-router-dom';

const ExerciseCard = props => {
    const [ gender, setGender ] = useState('female');
    const femaleImage = props.exercise.female.image;
    const maleImage = props.exercise.male.image;

    const changeGender = (gender, e) => {
        e.preventDefault();
        setGender(gender);
    }

    return (
        <div className="exercise-card">
            <Link to={{
                pathname: `/exercise/${props.exercise.id}`,
                state: { exercise: props.exercise, gender: gender }
            }}>
                <div className="exercise-card__image">
                    <img src={ gender === 'female' ? femaleImage : maleImage } alt={ props.exercise.name } />

                    <div className="exercise-card__options">
                        <button className={`button ${ gender === 'female' ? 'active' : ''}`} onClick={(e) => changeGender('female', e)}>Female</button>
                        <button className={`button ${ gender === 'male' ? 'active' : ''}`} onClick={(e) => changeGender('male', e)}>Male</button>
                    </div>
                </div>

                <div className="exercise-card__info">
                    <p>{ props.exercise.name }</p>
                    <span>{ props.exercise.bodyAreas.join(', ') }</span>
                </div>
            </Link>
        </div>
    );
};

export default ExerciseCard;