
import React, {useEffect, useContext} from 'react';

import { Context } from '../App/Provider'
import ExerciseCard from './ExerciseCard';


const Exercises = () => {
    const [state, dispatch] = useContext(Context);

    // GET DATA AND DISPATCH INTO GLOBAL STATE
    useEffect(() => {
        fetch('https://private-922d75-recruitmenttechnicaltest.apiary-mock.com/customexercises/')
            .then(response => {
                response.json().then(data => {
                    dispatch({type: 'SET_EXERCISES', payload: data.exercises});
                });
            })
            .catch(error => {
                dispatch({type: 'SET_ERROR', payload: error});
            });
    }, []);

    // LOADER
    let exercises = <p>Loading...</p>;

    // ERROR MESSAGE
    if (state.error) {
        exercises = <p>Sorry! Something went wrong: <span>{state.error}</span></p>;
    }

    // IF THERE IS NO ERROR AND WE HAVE EXERCISES
    if (!state.error && state.exercises) {
        exercises = state.exercises.map((exercise, index) => {
            return <ExerciseCard exercise={exercise} key={index} />;
        });
    }

    return (
        <main className="container">
            <section className="exercise-grid">
                { exercises }
            </section>
        </main>
    );
};


export default Exercises;