import React, { useState } from 'react';
import { Link, useLocation } from "react-router-dom";
import ReactHtmlParser from 'react-html-parser';

const Exercise = _ => {
    const { state } = useLocation();
    const [ gender, setGender ] = useState(state.gender);
    const femaleImage = state.exercise.female.image;
    const maleImage = state.exercise.male.image;

    const changeGender = gender => {
        setGender(gender);
    }

    return (
        <main className="container">
            <ul className="breadcrumbs">
                <li>
                    <Link to="/">Exercises</Link>
                </li>
                <li>
                    { state.exercise.name }
                </li>
            </ul>

            <section className="exercise">
                <div className="exercise__image">
                    <img src={ gender === 'female' ? femaleImage : maleImage } alt={ state.exercise.name } />

                    <div className="exercise__options">
                        <button className={`button ${ gender === 'female' ? 'active' : ''}`} onClick={() => changeGender('female')}>Female</button>
                        <button className={`button ${ gender === 'male' ? 'active' : ''}`} onClick={() => changeGender('male')}>Male</button>
                    </div>
                </div>

                <div className="exercise__info">
                    <h1>{ state.exercise.name }</h1>
                    <span>{ state.exercise.bodyAreas.join(', ') }</span>

                    { ReactHtmlParser(state.exercise.transcript) }
                </div>
            </section>
        </main>
    );
};

export default Exercise;