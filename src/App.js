import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Provider from './components/App/Provider'
import Exercises from './components/Exercise/Exercises'
import Exercise from './components/Exercise/Exercise'

const App = () => {
    return (
        <Provider>
            <header className="header">
                <svg aria-labelledby="560ad922-ebbd-4ddc-b20d-0378898fb898" xmlns="http://www.w3.org/2000/svg" width="145" height="111" fill="none" viewBox="0 0 145 111"><title id="560ad922-ebbd-4ddc-b20d-0378898fb898">Gymshark Logo</title><path fill="#000" d="M0 0l104.026 17.59-42.34 64.842h-3.602l-5.898 5.845-6.209-5.845h-1.648l-5.987 5.943-5.942-5.943h-2.011l-5.954 5.953-6.138-5.953H0l24.167 9.405 6.502-3.088 3.649 7.064 1.825.7 7.458-3.682 3.911 8.221 1.543.518-5.473 8.517L144.025.001H0z"></path><path fill="#000" d="M17.102 24.983l42.354 10.042-18.214 1.997-24.14-12.04z"></path></svg>
            </header>
            
            <Switch>
                <Route path="/" component={Exercises} exact />
                <Route path="/exercise/:id" render={()=> <Exercise />} />
            </Switch>
        </Provider>
    );
};

export default App;